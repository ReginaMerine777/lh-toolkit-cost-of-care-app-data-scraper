import scrapy
import os
from scrap_cdm.items import scrapCDMItem
from bs4 import BeautifulSoup
import json

urls = [
     ["https://data.cms.gov/Medicare-Inpatient/Inpatient-Prospective-Payment-System-IPPS-Provider/yekz-wzdr", "",
      "Inpatient", "Medicare Data"],
     ["https://data.cms.gov/Medicare-Outpatient/Provider-Outpatient-Hospital-Charge-Data-by-APC-CY/cgdj-v6ht", "",
      "Outpatient", "Medicare Data"],

    ["https://data.cms.gov/Medicare-Inpatient/National-Summary-of-Inpatient-Charge-Data-by-Medic/5k9u-5g92", "", "Inpatient_Summary", "Medicare Data"],
    ["https://data.cms.gov/Medicare-Outpatient/National-Summary-of-Outpatient-Hospital-Charge-Dat/renk-ui58", "", "Outpatient_Summary", "Medicare Data"]

]
class MedicareDataSpider(scrapy.Spider):
    name = 'medicare'

    def start_requests(self):
        for url in urls:
            yield scrapy.Request(url[0], callback=self.parse,
                                  meta={'index': url[1], 'name': url[2], 'state': url[3]})

    def parse(self, response):
        records = []
        soup = BeautifulSoup(response.text, 'lxml')

        js = json.loads(soup.find("script",type="application/ld+json").string)
        distribution=js['distribution']
        distribution=distribution[0]
        contentUrl=distribution['contentUrl']
        yield from self.save(contentUrl, response.meta['name'], response.meta['name'], response.meta['state'], records)

    def save(self, url, hospitalId, name, state, records):
        filename = os.path.basename(url.split('?')[0])
        outdir = os.path.join("Data")
        if os.path.isdir(outdir) == False:
            os.mkdir(outdir)
        path = os.path.join(outdir, state)
        if os.path.isdir(path) == False:
            os.mkdir(path)
        directory = name
        path = os.path.join(path, directory)
        if os.path.isdir(path) == False:
            os.mkdir(path)
        path = os.path.join(path, filename)
        self.log("\n\n\n We got data! \n\n\n" + path)
        self.logger.info('Saving File %s', path)
        records = records
        record = {
            'filename': filename,
            'hospitalId': hospitalId,  # sub hospital name
            'name': name  # hospital name in overpass api
        }
        records.append(record)
        item = scrapCDMItem()
        item['url'] = url
        item['path'] = path
        item['state'] = state
        item['directory'] = directory
        item['records'] = records
        yield item
